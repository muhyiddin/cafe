<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('items')->insert([
            'invoice_id' => "1",
            'description' => "Item 1",
            'qty' => "2",
            'cost' => "2"
        ]);
        DB::table('items')->insert([
            'invoice_id' => "1",
            'description' => "Item 2",
            'qty' => "1",
            'cost' => "3"
        ]);
        DB::table('items')->insert([
            'invoice_id' => "1",
            'description' => "Item 3",
            'qty' => "3",
            'cost' => "2"
        ]);
        DB::table('items')->insert([
            'invoice_id' => "1",
            'description' => "Item 4",
            'qty' => "2",
            'cost' => "1"
        ]);
    }
}
