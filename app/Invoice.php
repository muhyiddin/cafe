<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    //

    function items(){
        return $this->hasMany('App\Item');
    }
}
