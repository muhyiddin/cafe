<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Item;
use Illuminate\Http\Request;

use App\Http\Requests;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($invoices)
    {
        //
        $items = Item::where('invoice_id', $invoices)->get();

        return response()->success(compact('items'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $invoiceId)
    {
        //

        $invoice = Invoice::find($invoiceId);

        $item = new Item;

        $item->invoice_id = $invoiceId;
        $item->description = $request->input('description');
        $item->qty = $request->input('qty');
        $item->cost = $request->input('cost');

        $item = $invoice->items()->save($item);

        return response()->success(compact('item'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($invoice, $item)
    {
        //





        $item = Item::where('invoice_id', $invoice)->find($item);

        return response()->success(compact('item'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $invoiceId, $itemId)
    {
        //


        $invoice = Invoice::find($invoiceId);

        $item = Item::find($itemId);

        $item->description = $request->input('description');
        $item->qty = $request->input('qty');
        $item->cost = $request->input('cost');

        $item = $invoice->items()->save($item);


        return response()->success(compact('item'));



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($invoiceId, $itemId)
    {
        //
        $item = Item::where('invoice_id', $invoiceId)->find($itemId);

        return response()->success(compact('item'));
    }
}
