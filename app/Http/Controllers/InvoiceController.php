<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Item;
use Illuminate\Http\Request;

use App\Http\Requests;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $invoices = Invoice::get();

        return response()->success(compact('invoices'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $invoice = new Invoice;

        $invoice->customer = $request->input('customer');

        return response()->success(compact('invoice'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $invoice = Invoice::with('items')->find($id);



        return response()->success(compact('invoice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    //public function update(Invoice $invoice, Item $item)
    {
        //


        //$newItems = $request->items;
        //$items = $request->items;

        /*
        foreach ($items as $item) {
            $newItems =  $item->id;
        }
        */




        //$newItems =  $request->input('items');
        return $request;


        /*
        $invoice = Invoice::with('items')->find($id);
        $invoice->customer = $request->input('customer');
        $invoice->save();

        return response()->success(compact('invoice'));
        */


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $invoice = Invoice::find($id);
        $invoice->delete();
        return response()->success(compact('invoice'));
    }
}
