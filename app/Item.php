<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    //
    protected $hidden = array('created_at', 'updated_at');

    //protected $touches = ['invoice'];


    function invoice(){
        return $this->belongsTo('App\Invoice');
    }
}
