//import '../../../../bower_components/jquery/dist/jquery';
//import '../../../../bower_components/bootstrap/dist/js/bootstrap';
class InvoiceFormController{
    constructor(API, $log){
        'ngInject';

        //
        this.API = API;

        this.$log = $log;

        this.InvoiceID = 1;

        //this.baseInvoice  = [];
        this.baseInvoice = this.API.one('invoices',this.InvoiceID);
        this.invoice = [];


    }

    $onInit(){
        //this.baseInvoice = this.API.one('invoices',this.InvoiceID);

        this.baseInvoice.get().then((response) => {
            this.invoice = response.data.invoice;
        });


    }

    total(){
        var total=0;
        var items = this.invoice.items;
        for (let key in items) {
            let item = items[key];
            total += item.qty * item.cost;
        }
        return total;
    }

    addItem(){
        this.invoice.items.push({
            invoice_id: this.InvoiceID,
            cost: 1,
            qty:1
        });
        this.$log.info(this.invoice.items);

        
    }

    removeItem(hashKey, index) {
        this.$log.info(this.invoice.items);
        if (this.invoice.items.$$hashKey = hashKey){
            this.invoice.items.splice(index,1);
        }
        //this.invoice.items.splice(index, 1);

        this.$log.info('Deleted Item :' + hashKey);
    }

    save(data){

        this.$log.info('Ini data yang dikirim');
        this.$log.info(data);

        //var editData = this.baseInvoice;


        this.baseInvoice.put(data).then((response) => {
            //this.invoice = response.data.invoices;
            this.$log.warn('Ini data yg sampe');
            this.$log.warn(response);

        });


        this.$log.info('Save Clicked');

        //this.baseInvoice.put(data);

    }
}

export const InvoiceFormComponent = {
    templateUrl: './views/app/components/invoice-form/invoice-form.component.html',
    controller: InvoiceFormController,
    controllerAs: 'vm',
    bindings: {}
};
